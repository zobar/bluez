package bluez

import "github.com/zobar/svc"

type AdapterProps interface {
	// [readonly] The Bluetooth device address.
	Address() string

	// [readonly] The Bluetooth class of device.
	Class() uint32

	// [readonly] List of device object paths.
	Devices() []svc.Obj

	// [readwrite] Switch an adapter to discoverable or non-discoverable to
	// either make it visible or hide it. This is a global setting and should
	// only be used by the settings application.
	//
	// If the DiscoverableTimeout is set to a non-zero value then the system
	// will set this value back to false after the timer expired.
	//
	// In case the adapter is switched off, setting this value will fail.
	//
	// When changing the Powered property the new state of this property will be
	// updated via a PropertyChanged signal.
	Discoverable() bool

	// [readwrite] The discoverable timeout in seconds. A value of zero means
	// that the timeout is disabled and it will stay in discoverable/limited
	// mode forever.
	//
	// The default value for the discoverable timeout should be 180 seconds (3
	// minutes).
	DiscoverableTimeout() uint32

	// [readonly] Indicates that a device discovery procedure is active.
	Discovering() bool

	// [readwrite] The Bluetooth friendly name. This value can be changed and a
	// PropertyChanged signal will be emitted.
	Name() string

	// [readwrite] Switch an adapter to pairable or non-pairable. This is a
	// global setting and should only be used by the settings application.
	//
	// Note that this property only affects incoming pairing requests.
	Pairable() bool

	// [readwrite] The pairable timeout in seconds. A value of zero means that
	// the timeout is disabled and it will stay in pareable mode forever.
	PairableTimeout() uint32

	// [readwrite] Switch an adapter on or off. This will also set the
	// appropriate connectable state.
	Powered() bool

	// [readonly] List of 128-bit UUIDs that represents the available local
	// services.
	UUIDs() []string
}

type adapterProps struct{ props map[string]interface{} }

func adapterPropsResult(in svc.Result) (out AdapterProps, err error) {
	if err = in.Err(); err == nil {
		out = &adapterProps{in.Val(0).(map[string]interface{})}
	}
	return
}

func (props *adapterProps) Address() string {
	return props.props["Address"].(string)
}

func (props *adapterProps) Class() uint32 {
	return props.props["Class"].(uint32)
}

func (props *adapterProps) Devices() []svc.Obj {
	return props.props["Devices"].([]svc.Obj)
}

func (props *adapterProps) Discoverable() bool {
	return props.props["Discoverable"].(bool)
}

func (props *adapterProps) DiscoverableTimeout() uint32 {
	return props.props["DiscoverableTimeout"].(uint32)
}

func (props *adapterProps) Discovering() bool {
	return props.props["Discovering"].(bool)
}

func (props *adapterProps) Name() string {
	return props.props["Name"].(string)
}

func (props *adapterProps) Pairable() bool {
	return props.props["Pairable"].(bool)
}

func (props *adapterProps) PairableTimeout() uint32 {
	return props.props["PairableTimeout"].(uint32)
}

func (props *adapterProps) Powered() bool {
	return props.props["Powered"].(bool)
}

func (props *adapterProps) UUIDs() []string {
	return props.props["UUIDs"].([]string)
}
