package bluez

import "github.com/zobar/svc"

type ManagerProps interface {
	// [readonly] List of adapter object paths.
	Adapters() ([]Adapter, error)
}

type managerProps struct {
	props map[string]interface{}
}

func managerPropsResult(in svc.Result) (out ManagerProps, err error) {
	if err = in.Err(); err == nil {
		out = &managerProps{in.Val(0).(map[string]interface{})}
	}
	return
}

func (props *managerProps) Adapters() ([]Adapter, error) {
	return asAdapters(props.props["Adapters"].([]svc.Obj))
}
