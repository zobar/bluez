package bluez

import "github.com/zobar/svc"

func adapterHandler(obj svc.Obj, handler func(Adapter)) func([]interface{}) {
	return func(args []interface{}) {
		if adapter, err := asAdapter(obj.Svc().Obj(args[0].(string))); err == nil {
			handler(adapter)
		}
	}
}

func propChangeHandler(handler func(string, interface{})) func([]interface{}) {
	return func(args []interface{}) {
		handler(args[0].(string), args[1])
	}
}
