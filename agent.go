package bluez

import "github.com/guelfey/go.dbus"

type Agent interface {
	// This method gets called when the service daemon needs to authorize a
	// connection/service request.
	Authorize(device dbus.ObjectPath, uuid string) error

	// This method gets called to indicate that the agent request failed before
	// a reply was returned.
	Cancel() error

	// This method gets called if a mode change is requested that needs to be
	// confirmed by the user. An example would be leaving flight mode.
	ConfirmModeChange(mode string) error

	// This method gets called when the service daemon needs to display a
	// passkey for an authentication.
	//
	// The entered parameter indicates the number of already typed keys on the
	// remote side.
	//
	// An empty reply should be returned. When the passkey needs no longer to be
	// displayed, the Cancel method of the agent will be called.
	//
	// During the pairing process this method might be called multiple times to
	// update the entered value.
	//
	// Note that the passkey will always be a 6-digit number, so the display
	// should be zero-padded at the start if the value contains less than 6
	// digits.
	DisplayPasskey(device dbus.ObjectPath, passkey uint32, entered uint8) error

	// This method gets called when the service daemon unregisters the agent.
	// An agent can use it to do cleanup tasks. There is no need to unregister
	// the agent, because when this method gets called it has already been
	// unregistered.
	Release() error

	// This method gets called when the service daemon needs to confirm a
	// passkey for an authentication.
	//
	// To confirm the value it should return an empty reply or an error in case
	// the passkey is invalid.
	//
	// Note that the passkey will always be a 6-digit number, so the display
	// should be zero-padded at the start if the value contains less than 6
	// digits.
	RequestConfirmation(device dbus.ObjectPath, passkey uint32) error

	// This method gets called when the service daemon needs to get the passkey
	// for an authentication.
	//
	// The return value should be a numeric value between 0-999999.
	RequestPasskey(device dbus.ObjectPath) (uint32, error)

	// This method gets called when the service daemon needs to get the passkey
	// for an authentication.
	//
	// The return value should be a string of 1-16 characters length. The string
	// can be alphanumeric.
	RequestPinCode(device dbus.ObjectPath) (string, error)
}
