package bluez

import (
	"fmt"
	"github.com/guelfey/go.dbus"
)

type agentIface struct {
	agent Agent
	path  dbus.ObjectPath
}

func convertErr(in error) (out *dbus.Error) {
	if in != nil {
		out = &dbus.Error{Name: in.Error()}
	}
	return
}

func (iface *agentIface) Authorize(device dbus.ObjectPath, uuid string) *dbus.Error {
	fmt.Println("Interface Authorize", device, uuid)
	return convertErr(iface.agent.Authorize(device, uuid))
}

func (iface *agentIface) Cancel() *dbus.Error {
	fmt.Println("Interface Cancel")
	return convertErr(iface.agent.Cancel())
}

func (iface *agentIface) ConfirmModeChange(mode string) *dbus.Error {
	fmt.Println("Interface ConfirmModeChange", mode)
	return convertErr(iface.agent.ConfirmModeChange(mode))
}

func (iface *agentIface) DisplayPasskey(device dbus.ObjectPath, passkey uint32, entered uint8) *dbus.Error {
	fmt.Println("Interface DisplayPasskey", device, passkey, entered)
	return convertErr(iface.agent.DisplayPasskey(device, passkey, entered))
}

func (iface *agentIface) Release() *dbus.Error {
	fmt.Println("Interface Release")
	return convertErr(iface.agent.Release())
}

func (iface *agentIface) RequestConfirmation(device dbus.ObjectPath, passkey uint32) *dbus.Error {
	fmt.Println("Interface RequestConfirmation", device, passkey)
	return convertErr(iface.agent.RequestConfirmation(device, passkey))
}

func (iface *agentIface) RequestPasskey(device dbus.ObjectPath) (uint32, *dbus.Error) {
	fmt.Println("Interface RequestPasskey", device)
	passkey, err := iface.agent.RequestPasskey(device)
	return passkey, convertErr(err)
}

func (iface *agentIface) RequestPinCode(device dbus.ObjectPath) (string, *dbus.Error) {
	fmt.Println("Interface RequestPinCode", device)
	pin, err := iface.agent.RequestPinCode(device)
	return pin, convertErr(err)
}

func newAgentIface(blueZ BlueZ, agent Agent) (iface *agentIface, err error) {
	iface = &agentIface{agent: agent}
	// iface.path, err = daemon.export(iface, "org.bluez.Agent")
	return
}
