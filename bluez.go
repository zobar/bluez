package bluez

import "github.com/zobar/svc"

type (
	BlueZ interface {
		Manager() (mgr Manager, err error)
	}

	blueZ struct{ svc svc.Svc }
)

func Daemon() BlueZ {
	return &blueZ{svc.Sys().Svc("org.bluez")}
}

// func (daemon *Daemon) export(object interface{}, ifaceName string) (path dbus.ObjectPath, err error) {
// 	conn, err := daemon.conn()
// 	if err == nil {
// 		path = (dbus.ObjectPath)(fmt.Sprintf("/go/%d/%#p", os.Getpid(), object))
// 		err = conn.Export(object, path, ifaceName)
// 	}
// 	return
// }

func (blueZ *blueZ) Manager() (mgr Manager, err error) {
	return asManager(blueZ.svc.Obj("/"))
}
