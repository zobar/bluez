package bluez

import "github.com/zobar/svc"

type Manager interface {
	// Parameter is object path of added adapter.
	AdapterAdded(func(adapter Adapter))

	// Parameter is object path of removed adapter.
	AdapterRemoved(func(adapter Adapter))

	// Returns object path for the default adapter.
	DefaultAdapter() (Adapter, error)

	// Parameter is object path of the new default adapter.
	//
	// In case all adapters are removed this signal will not be emitted. The
	// AdapterRemoved signal has to be used to detect that no default adapter is
	// selected or available anymore.
	DefaultAdapterChanged(func(adapter Adapter))

	// Returns object path for the specified adapter. Valid patterns are "hci0"
	// or "00:11:22:33:44:55".
	FindAdapter(pattern string) (Adapter, error)

	// Returns all global properties. See the properties section for available
	// properties.
	GetProperties() (ManagerProps, error)

	// This signal indicates a changed value of the given property.
	PropertyChanged(func(name string, value interface{}))
}

type manager struct{ svc.Iface }

func asManager(obj svc.Obj) (mgr Manager, err error) {
	iface, err := obj.As("org.bluez.Manager")
	if err == nil {
		mgr = &manager{iface}
	}
	return
}

func (mgr manager) AdapterAdded(handler func(Adapter)) {
	mgr.On("AdapterAdded", adapterHandler(mgr.Obj(), handler))
}

func (mgr manager) AdapterRemoved(handler func(Adapter)) {
	mgr.On("AdapterRemoved", adapterHandler(mgr.Obj(), handler))
}

func (mgr manager) DefaultAdapter() (Adapter, error) {
	return adapterResult(mgr.Call("DefaultAdapter"))
}

func (mgr manager) DefaultAdapterChanged(handler func(Adapter)) {
	mgr.On("DefaultAdapterChanged", adapterHandler(mgr.Obj(), handler))
}

func (mgr manager) FindAdapter(pattern string) (Adapter, error) {
	return adapterResult(mgr.Call("FindAdapter", pattern))
}

func (mgr manager) GetProperties() (ManagerProps, error) {
	return managerPropsResult(mgr.Call("GetProperties"))
}

func (mgr manager) PropertyChanged(handler func(string, interface{})) {
	mgr.On("PropertyChanged", propChangeHandler(handler))
}
