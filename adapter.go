package bluez

import "github.com/zobar/svc"

type Adapter interface {
	// Returns all properties for the adapter. See the properties section for
	// available properties.
	GetProperties() (AdapterProps, error)

	// This signal indicates a changed value of the given property.
	PropertyChanged(handler func(name string, value interface{}))

	// This registers the adapter wide agent.
	//
	// The object path defines the path of the agent that will be called when
	// user input is needed.
	//
	// If an application disconnects from the bus all of its registered agents
	// will be removed.
	//
	// The capability parameter can have the values "DisplayOnly",
	// "DisplayYesNo", "KeyboardOnly" and "NoInputNoOutput" which reflects the
	// input and output capabilities of the agent. If an empty string is used it
	// will fallback to "DisplayYesNo".
	// RegisterAgent(agent Agent, capability string) VoidCall

	// Changes the value of the specified property. Only properties that are
	// listed a read-write are changeable. On success this will emit a
	// PropertyChanged signal.
	SetProperty(name string, value interface{}) error

	// This method starts the device discovery session. This includes an inquiry
	// procedure and remote device name resolving. Use StopDiscovery to release
	// the sessions acquired.
	//
	// This process will start emitting DeviceFound and PropertyChanged
	// "Discovering" signals.
	StartDiscovery() error

	// This method will cancel any previous StartDiscovery transaction.
	//
	// Note that a discovery procedure is shared between all discovery sessions
	// thus calling StopDiscovery will only release a single session.
	StopDiscovery() error
}

type adapter struct{ svc.Iface }

func adapterResult(in svc.Result) (out Adapter, err error) {
	if err = in.Err(); err == nil {
		out, err = asAdapter(in.Val(0).(svc.Obj))
	}
	return
}

func asAdapter(obj svc.Obj) (result Adapter, err error) {
	iface, err := obj.As("org.bluez.Adapter")
	if err == nil {
		result = &adapter{iface}
	}
	return
}

func asAdapters(objs []svc.Obj) (adapters []Adapter, err error) {
	adapters = make([]Adapter, len(objs))
	for i, obj := range objs {
		if adapters[i], err = asAdapter(obj); err != nil {
			adapters = nil
			break
		}
	}
	return
}

func (adapter adapter) GetProperties() (AdapterProps, error) {
	return adapterPropsResult(adapter.Call("GetProperties"))
}

func (adapter adapter) PropertyChanged(handler func(string, interface{})) {
	adapter.On("PropertyChanged", propChangeHandler(handler))
}

// func (adapter adapter) RegisterAgent(agent Agent, capability string) *svc.Call {
// 	agentIface, err := adapter.obj.daemon.newAgentIface(agent)
// 	if err != nil {
// 		return adapter.obj.newVoidErr(err)
// 	}
// 	return adapter.obj.newsvc.Call("org.bluez.Adapter.RegisterAgent", agentIface.path, capability)
// }

func (adapter adapter) SetProperty(name string, value interface{}) error {
	return adapter.Call("SetProperty", name, svc.NewVariant(value)).Err()
}

func (adapter adapter) StartDiscovery() error {
	return adapter.Call("StartDiscovery").Err()
}

func (adapter adapter) StopDiscovery() error {
	return adapter.Call("StopDiscovery").Err()
}
